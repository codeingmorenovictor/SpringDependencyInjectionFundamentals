package com.dependencyInjection.processors;

import org.springframework.stereotype.Component;

import com.dependencyInjection.services.EmployeeService;

@Component("identifierNationalEmployees")
public class IdentifierCreatorNationalEmployees implements EmployeeService{

	@Override
	public String generateIdentifier() {
		return "This method helps us to understand DI through @Qualifier annotation";
	}

}
