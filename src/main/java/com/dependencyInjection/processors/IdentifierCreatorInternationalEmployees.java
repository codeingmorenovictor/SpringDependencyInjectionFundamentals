package com.dependencyInjection.processors;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.dependencyInjection.services.EmployeeService;

@Component("identifierInternationalEmployees")
@Primary 
public class IdentifierCreatorInternationalEmployees implements EmployeeService{

	@Override
	public String generateIdentifier() {
		return "This method helps us to understand DI through @Primary annotation";
	}

}
