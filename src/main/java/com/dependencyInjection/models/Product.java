package com.dependencyInjection.models;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class Product implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8751771148284415530L;

	private Integer serial;
	private String name;
	private BigDecimal price;

	public Product(Integer serial, String name, BigDecimal price) {
		this.serial = serial;
		this.name = name;
		this.price = price;
	}

}
