package com.dependencyInjection.models;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@EqualsAndHashCode
@Component
//@Singleton The Scope By Default, lasts while the app and server are up
//@RequestScope Lasts a short time, only while the HTTP request exists 
//@SessionScope Lasts till the browser is closed, a time out happens or session is invalidated MUST IMPLEMENT SERIALIZABLE
public class Invoice implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2070809204867860587L;

	private Integer serial;
	private String description;
	@Autowired
	private Customer customer;

	@Autowired // Injected using Beans configuration File
	private List<ItemInvoice> items;

	@PostConstruct
	public void init() {
		customer.setName(customer.getName().concat(" ").concat("Moreno R."));
		// Here some code executed as soon as the project is built
	}
	
	//By by default the scope is @Singleton it is to destroy till the app goes down
	//IT DOESN�T WORK IN @SESSIONSCOPE SCOPE
	@PreDestroy
	public void destroy() {
		//Here some code to be executed PreDestroy according to the specified scope
		log.info("@PreDestroy in action, by default the scope @Singleton is to destroy till the app goes down");
	}

}
