package com.dependencyInjection.models;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class ItemInvoice implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7397460669881352566L;

	private Integer itemNumber;
	private Product product;
	private BigDecimal quantity;

	public ItemInvoice(Integer itemNumber, Product product, BigDecimal quantity) {
		this.itemNumber = itemNumber;
		this.product = product;
		this.quantity = quantity;
	}

}
