package com.dependencyInjection.models;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@Component
public class Customer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3783724086814282704L;
	@Value("${customer.rfc}")
	private String rfc;
	
	@Value("${customer.name}")
	private String name;

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
