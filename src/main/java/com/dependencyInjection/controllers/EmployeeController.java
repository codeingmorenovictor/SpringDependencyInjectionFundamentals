package com.dependencyInjection.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.dependencyInjection.services.EmployeeService;

@Controller
public class EmployeeController {

	/*
	 * In this case we have two classes that implements EmployeeService (), each one
	 * describes a particular behavior for the EmployeeService methods, we must
	 * annotate using @Primary or @Qualifier to specify which behavior we want to
	 * use. Error: Field employeeService in
	 * com.dependencyInjection.controllers.EmployeeController required a single
	 * bean, but 2 were found
	 */
	@Autowired
	private EmployeeService employeeServicePrimary; // If not @Qualifier, it uses @Primary implementation

	@Autowired
	@Qualifier("identifierNationalEmployees") //Name of stereotype
	private EmployeeService employeeServiceQualifier;

	@GetMapping(value = { "/primary", "/", "" })
	public String indexPrimary(Model model) {
		model.addAttribute("header", "Dependency Injection using @Primary and @Qualifier annotations...");
		model.addAttribute("result", employeeServicePrimary.generateIdentifier());
		return "index";
	}

	@GetMapping("/qualifier")
	public String indexQualifier(Model model) {
		model.addAttribute("header", "Dependency Injection using @Primary and @Qualifier annotations...");
		model.addAttribute("result", employeeServiceQualifier.generateIdentifier());
		return "index";
	}

}
