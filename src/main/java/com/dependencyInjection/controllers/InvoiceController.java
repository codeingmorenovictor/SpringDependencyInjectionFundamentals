package com.dependencyInjection.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dependencyInjection.models.Invoice;

@Controller
@RequestMapping("/invoice")
public class InvoiceController {

	@Value("${invoice.header}")
	private String header;

	@Autowired
	private Invoice invoice;

	@GetMapping(value = { "/detail", "/", "" })
	public String invoiceDetail(Model model) {
		model.addAttribute("header", header);
		model.addAttribute("invoice", invoice);
		return "invoice";
	}

}
