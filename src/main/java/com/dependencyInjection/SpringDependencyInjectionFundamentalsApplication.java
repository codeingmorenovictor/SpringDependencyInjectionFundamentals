package com.dependencyInjection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDependencyInjectionFundamentalsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDependencyInjectionFundamentalsApplication.class, args);
	}

}
