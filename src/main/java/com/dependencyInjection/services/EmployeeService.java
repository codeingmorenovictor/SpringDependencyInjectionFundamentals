package com.dependencyInjection.services;

public interface EmployeeService {

	public String generateIdentifier();
}
