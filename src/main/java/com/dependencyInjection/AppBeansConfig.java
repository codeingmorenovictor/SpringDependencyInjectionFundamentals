package com.dependencyInjection;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.dependencyInjection.models.ItemInvoice;
import com.dependencyInjection.models.Product;

@Configuration
public class AppBeansConfig {

	@Bean("itemInvoicePrimary")
	public List<ItemInvoice> addItemsPrimary() {
		Product p1 = new Product(145, "Professional Camera", new BigDecimal(1245.25));
		Product p2 = new Product(189, "Java course", new BigDecimal(19000));
		Product p3 = new Product(145, "Jordan Tennis Shoes", new BigDecimal(2100.50));

		ItemInvoice i1 = new ItemInvoice(12000, p1, new BigDecimal(200));
		ItemInvoice i2 = new ItemInvoice(120478, p2, new BigDecimal(170));
		ItemInvoice i3 = new ItemInvoice(478955, p3, new BigDecimal(350));

		return Arrays.asList(i1, i2, i3);
	}

	@Bean("itemInvoiceQualifier")
	@Primary
	public List<ItemInvoice> addItemsQualifier() {

		Product p1 = new Product(145, "Monitor", new BigDecimal(147.50));
		Product p2 = new Product(210, "Mouse Pad", new BigDecimal(80.75));
		Product p3 = new Product(145, "Keyboard", new BigDecimal(100.50));
		Product p4 = new Product(178, "Charger", new BigDecimal(75));
		Product p5 = new Product(891, "Laptop", new BigDecimal(12000));

		ItemInvoice i1 = new ItemInvoice(15001, p1, new BigDecimal(215));
		ItemInvoice i2 = new ItemInvoice(12587, p2, new BigDecimal(170));
		ItemInvoice i3 = new ItemInvoice(78985, p3, new BigDecimal(10));
		ItemInvoice i4 = new ItemInvoice(12789, p4, new BigDecimal(410));
		ItemInvoice i5 = new ItemInvoice(36999, p5, new BigDecimal(20));
		return Arrays.asList(i1, i2, i3, i4, i5);

	}

}
